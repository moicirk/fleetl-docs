# Fleetl
Fleetl is a Taxify for the beauty industry and includes several technical services:

* API backend
* Hybrid mobile application for clients
* Hybrid mobile application for masters
* Shared library

### API
API projects present whole business logic, database connection and GraphQL endpoints for clients and masters. All Fleetl interaction goes through this API.

**Stack:** PHP >7, Yii2 framework, MySQL 5.7, Redis >4

### Client application
A hybrid mobile application that provides an interface for users to request beauty services, see masters portfolio and manage all requests made.

**Stack:** javascript, Cordova 9, VueJS 2.5, Onsen UI 2

### Master application
A hybrid mobile application that provides an interface for users to receive client requests, make offers and manage profile and portfolio.

**Stack:** javascript, Cordova 9, VueJS 2.5, Onsen UI 2

### Shared library
The shared library used in mobile applications projects as git `submodule` and presents components, helpers, styles, etc. that used in both applications.
**Stack:** javascript, Cordova 9, VueJS 2.5, Onsen UI 2

## Contribution

### Project and ticket management
Fleetl team uses Trello boards to manage working tickets and releases. To get
access to boards create Trello account and ask an admin to invite you to Fleetl project.
We plan and develop the product in 2-week iterations. At the end of every iteration we have a team meeting to discuss and plan the next iteration.
Our main working board called `Development` and contains several ticket lists:

* `Backlog` contains tickets that are not yet discussed or planned for future development. Usually, we do not work on them in the current iteration.
* `ToDo` contains ticket discussed and planned for the current iteration and could be taken to work. The priority goes from top-to-bottom. On the top we place the most important things that must be done in the first order
* `In progress` tickets that are currently in work. Every ticket there must be assigned to a concrete developer (avatar is visible on the card), so the team knows who is working on it.
* `In review` when a ticket is done it must be reviewed by any developer except executor.
* `Done` tickets that passed the review process and concerned as done

We also use several labels and card types to categorize tickets and work.
* `epic` usually card with this label is just a container that contains an explanation of feature or module that cannot be done fast with a single ticket. This ticket type is not going to `In progress` because it is not a job, but a container or group of tickets.
* `API` job related to API service
* `master-app` job related to master application service
* `client-app` job related to client application service
* `admin` job related to administration interface included in `API` project

Sometimes you can see a label with just number, like `1`, `3`, `5` etc. This is a story score which shows the complexity of the job. Counting the summary of these scores at the end of every iteration we can define the speed of the team and use it to plan next sprint accurately.

Any label could be used in any combination, sometimes jobs related to API or different applications at the same time.

### Workflow
We are keeping the following workflow in an advance:

* Take a ticket from `ToDo` list and read description carefully
* Discuss details in slack `development` channel if you have questions or problems related to job
* Move ticket to `In progress` list
* Assign yourself to ticket
* Create a new `git` branch from `master` or `develop` (in API project only) using ticket humanized name. You can find this name opening a ticket in Trello right in browser URL address input

![Trello ticket name to branch](/assets/trello-ticket-name.png)

```bash
git checkout -b 437-client-email-is-unique // creates new branch
git push -u origin HEAD // pushes a newly created branch to the server
```

* Start to work, make commits
* When work is done go to bitbucket and create a pull request. 
[How to create a pull request on Bitbucket](https://confluence.atlassian.com/bitbucket/create-a-pull-request-to-merge-your-change-774243413.html)
* Attach pull request to Trello ticket. [How to attach PR](https://help.trello.com/article/1082-using-the-bitbucket-power-up)
* Move ticket to `In review` list
* When a ticket is successfully reviewed and approved, go to Bitbucket and push the `Merge` button to merge your code to main project branch
* Move card to `Done` list

### Getting access to project code
We use `Bitbucket` as the main git server. Project is private and can be accessed only by admin approval. To get access you must first generate `SSH` key, read here how to do it
https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html

Then give your public key to admin to add to a list of access keys.

[Start with API project](https://bitbucket.org/ganstashit/api/src/develop/README.md)
[Start with client app project](https://bitbucket.org/ganstashit/client-app/src/master/README.md)
[Start with mobile app project](https://bitbucket.org/ganstashit/business-app/src/master/README.md)
